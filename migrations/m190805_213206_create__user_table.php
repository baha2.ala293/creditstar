<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%_user}}`.
 */
class m190805_213206_create__user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->createTable('user', [
            'id' => Schema::TYPE_PK,
            'first_name' => Schema::TYPE_STRING . ' NOT NULL',
            'last_name' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'personal_code' => Schema::TYPE_BIGINT . ' NOT NULL',
            'phone' => Schema::TYPE_BIGINT . ' NOT NULL',
            'active' => Schema::TYPE_BOOLEAN,
            'dead' => Schema::TYPE_BOOLEAN,
            'lang' => Schema::TYPE_STRING
        ]);

        $this->createIndex('idx-user-email','user','email');

        $this->createIndex('idx-user-phone','user','phone');

        $this->createIndex('idx-user-personal-code','user','personal_code');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx-user-personal-code','user');
        $this->dropIndex('idx-user-phone','user');
        $this->dropIndex('idx-user-email','user');

        $this->dropTable('user');
    }
}
