<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%loan}}`.
 */
class m190805_213750_create_loan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('loan', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER .' NOT NULL',
            'amount' => Schema::TYPE_BIGINT .' NOT NULL',
            'interest' => Schema::TYPE_BIGINT .' NOT NULL',
            'duration' => Schema::TYPE_INTEGER . ' NOT NULL',
            'start_date' => Schema::TYPE_DATE . ' NOT NULL',
            'end_date' => Schema::TYPE_DATE . ' NOT NULL',
            'campaign' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN
        ]);

        $this->createIndex('idx-loan-user_id','loan','user_id');

        $this->addForeignKey(
            'fk-loan-user_id',
            'loan',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-loan-user_id','loan');

        $this->dropIndex('idx-loan-user_id','loan');

        $this->dropTable('loan');
    }
}
